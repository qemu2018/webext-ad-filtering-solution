/*
 * This file is part of eyeo's Web Extension Ad Blocking Toolkit (EWE),
 * Copyright (C) 2006-present eyeo GmbH
 *
 * EWE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * EWE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EWE.  If not, see <http://www.gnu.org/licenses/>.
 */

import browser from "webextension-polyfill";
import {v4 as uuidv4} from "uuid";
import {MILLIS_IN_HOUR} from "adblockpluscore/lib/time.js";

import * as info from "./info.js";
import {hasAcceptableAdsEnabled} from "./subscriptions.js";
import {EventDispatcher} from "./types.js";
import {Scheduler} from "./scheduler.js";
import {default as initializer} from "./initializer.js";

const TELEMETRY_STORAGE_KEY = "ewe:telemetry";

let scheduler = null;
let onError = new EventDispatcher(dispatch => {});

/**
 * @ignore
 * Starts the telemetry
 *
 * @param {Object} telemetry Telemetry configuration (URL, bearer)
 * @param {number} [interval]
 *        Ping interval in milliseconds (default: 12 hours)
 * @param {number} [errorRetryDelay]
 *        Error retry interval in milliseconds (default: 1 hour)
 */
export function start(telemetry, interval = 12 * MILLIS_IN_HOUR,
                      errorRetryDelay = 1 * MILLIS_IN_HOUR) {
  if (!telemetry.url) {
    throw new Error("No telemetry `url` provided");
  }

  if (!telemetry.bearer) {
    throw new Error("No telemetry `bearer` provided");
  }

  if (scheduler) {
    return;
  }

  scheduler = new Scheduler({
    interval,
    errorRetryDelay,
    listener: () => ping(telemetry),
    async getNextTimestamp() {
      let storageResult = await browser.storage.local.get(
        [TELEMETRY_STORAGE_KEY]
      );
      let storage = storageResult[TELEMETRY_STORAGE_KEY];
      let lastPing = storage && storage.lastPing;
      let lastError = storage && storage.lastError;

      if (lastError) {
        let nextRetryAfterError = new Date(lastError).getTime() +
            errorRetryDelay;
        let nowTimestamp = Date.now();
        let maxNextRetry = nowTimestamp + errorRetryDelay;
        if (nextRetryAfterError > maxNextRetry) {
          // This shouldn't happen if the clocks are working right. It implies
          // that the system clock has been set backwards since we stored that
          // timestamp. Let's correct the storage using our current clock
          // value. We shouldn't do this with lastPing because lastPing comes
          // from the server.
          nextRetryAfterError = maxNextRetry;
          await browser.storage.local.set({
            [TELEMETRY_STORAGE_KEY]: {
              ...storage,
              lastError: (new Date(nowTimestamp)).toISOString()
            }
          });
        }

        return nextRetryAfterError;
      }
      else if (lastPing) {
        return new Date(lastPing).getTime() + interval;
      }

      return null;
    }
  });
}

/**
 * @ignore
 */
export async function reset() {
  stop();
  await browser.storage.local.remove(TELEMETRY_STORAGE_KEY);
}

/**
 * @ignore
 */
export function stop() {
  if (!scheduler) {
    return;
  }

  scheduler.stop();
  scheduler = null;
}

function truncateTimes(timestamp) {
  if (!timestamp) {
    return timestamp;
  }

  return timestamp.split("T")[0] + "T00:00:00Z";
}

async function ping(telemetry) {
  await initializer.start();

  const storageResult = await browser.storage.local.get(
    [TELEMETRY_STORAGE_KEY]
  );
  const storage = storageResult[TELEMETRY_STORAGE_KEY];

  let payload = await getExtensionMetadata();
  if (storage) {
    payload.first_ping = truncateTimes(storage.firstPing);
    payload.last_ping = truncateTimes(storage.lastPing);
    payload.previous_last_ping = truncateTimes(storage.previousLastPing);
    payload.last_ping_tag = storage.lastPingTag;
  }

  let pingToken;
  try {
    pingToken = await fetchWithBearerToken(
      telemetry.url,
      telemetry.bearer,
      {payload}
    );
  }
  catch (e) {
    let lastError = (new Date()).toISOString();
    await browser.storage.local.set({
      [TELEMETRY_STORAGE_KEY]: {
        ...storage,
        lastError
      }
    });

    onError.emit({
      message: e.message,
      lastError
    });
    return false;
  }

  await browser.storage.local.set({
    [TELEMETRY_STORAGE_KEY]: {
      firstPing: (storage && storage.firstPing) || pingToken,
      lastPing: pingToken,
      previousLastPing: storage && storage.lastPing,
      lastPingTag: uuidv4()
    }
  });
  return true;
}

async function fetchWithBearerToken(url, bearer, body) {
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Authorization": `Bearer ${bearer}`,
      "Content-Type": "application/json"
    },
    body: JSON.stringify(body)
  });

  if (!response.ok) {
    throw new Error(
      `Telemetry server responded with error status ${response.status}.`
    );
  }

  const data = await response.json();
  let pingToken = data.token;

  if (!pingToken) {
    throw new Error("Telemetry server response did not include a token.");
  }

  return pingToken;
}

async function getExtensionMetadata() {
  let platform;
  let platformVersion;

  if (navigator.userAgentData) {
    const platformInfo = await navigator.userAgentData.getHighEntropyValues([
      "platform",
      "platformVersion"
    ]);

    platform = platformInfo.platform;
    platformVersion = platformInfo.platformVersion;
  }
  else {
    const platformInfo = await browser.runtime.getPlatformInfo();
    platform = platformInfo.os;
  }

  return {
    // The OS. Windows, Linux, etc.
    platform,

    // The version of the system OS.
    platform_version: platformVersion,

    // The browser. Chrome, Firefox, etc.
    application: info.application,

    // The version of the browser.
    application_version: info.applicationVersion,

    // Used to identify this specific project.
    addon_name: "eyeo-webext-ad-filtering-solution",

    // The Engine's current version. See the Dotenv plugin in webpack.config.js
    addon_version: webpackDotenvPlugin.VERSION,

    // The name of the extension using the Engine.
    extension_name: info.addonName,

    // The extension's version.
    extension_version: info.addonVersion,

    // Depends on having the acceptable ads subscription enabled.
    aa_active: await hasAcceptableAdsEnabled()
  };
}


export default {
  /**
  * @typedef {Object} TelemetryPingError
  * @property {string} message The error message, describing the failure.
  * @property {string} lastError The timestamp of the error.
  */

  /**
   * Emitted when sending a telemetry ping fails.
   *
   * @event
   * @type {EventDispatcher<TelemetryPingError>}
   */
  onError
};
